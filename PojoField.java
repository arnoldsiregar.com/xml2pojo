class PojoField {

    private String name;
    private String type;
    private Integer length;

    public PojoField(String name, String type, Integer length) {
        this.name = name;
        this.type = type;
        this.length = length;
    }

    public String getName() {
        return this.name;
    }

    public String getType() {
        return this.type;
    }

    public Integer getLength() {
        return this.length;
    }

    @Override
    public String toString() {
        return this.name + ", " + this.type + ", " + this.length;
    }
}