Compile:
    javac Xml2Pojo.java

Run:
1. Output to screen:
    java Xml2Pojo [xmlfile]
    ex: Xml2Pojo car.xml

2. Output to file:
    java Xml2Pojo [xmlfile] > [newfile]
    ex: Xml2Pojo car.xml > Car.java

XML File:
Root: element tag -> Class Name
Elements: 
    element tag -> fieldName, getter and setter
    type attribute -> datatype
    length attribute -> length (not used yet)