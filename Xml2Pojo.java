class Xml2Pojo {
    public static void main(String[] args) {
        XmlReader reader = new XmlReader();
        String arg = args.length > 0 ? args[0] : "schema.xml";
        reader.read(arg);
    }
}