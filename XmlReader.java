import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.*;

import org.w3c.dom.*;

class XmlReader {

    public HashMap<String, PojoField> read(String filePath) {

        HashMap<String, PojoField> result = new HashMap<>();

        File xmlFile = new File(filePath);

        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder;

        try {
            dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(xmlFile);
            doc.getDocumentElement().normalize();

            String root = doc.getDocumentElement().getNodeName();
            // System.out.println("Class: " + root);

            NodeList nodeList = doc.getElementsByTagName(root);

            Node node = nodeList.item(0);
            int nodeChildLength = node.getChildNodes().getLength();

            for (int i = 0; i < nodeChildLength; i++) {

                Node field = node.getChildNodes().item(i);
                if (field.getNodeType() == Node.ELEMENT_NODE) {
                    String nodeName = field.getNodeName();
                    Node nodeType = field.getAttributes().getNamedItem("type");
                    Node nodeLength = field.getAttributes().getNamedItem("length");

                    PojoField pojoField = new PojoField(nodeName, nodeType.getNodeValue(),
                            nodeLength != null ? Integer.parseInt(nodeLength.getNodeValue()) : -1);

                    result.put(nodeName, pojoField);
                }
            }

            // printResult(result);
            printClass(root, result);

        } catch (Exception e1) {
            e1.printStackTrace();
        }

        return result;
    }

    private void printResult(HashMap<String, PojoField> result) {
        // lets print object list information
        Set set = result.entrySet();
        Iterator iterator = set.iterator();

        while (iterator.hasNext()) {
            Map.Entry entry = (Map.Entry) iterator.next();
            System.out.println(entry.getKey() + ", " + entry.getValue());
        }
    }

    private void printClass(String className, HashMap<String, PojoField> fields) {
        String classTemplate = "public class [class_name] {\n" + "[fields_content]\n" + "[getter_content]\n"
                + "[setter_content]\n}";

        String fieldTemplate = "\tprivate [field_type] [field_name];\n";

        String getterTemplate = "\tpublic [field_type] get[field_name]() {\n" + "\t\treturn this.[field_name];\n"
                + "\t}\n";

        String setterTemplate = "\tpublic void set[field_name]([field_type] value) {\n"
                + "\t\tthis.[field_name] = value;\n" + "\t}\n";

        //
        String classText = "";
        String fieldText = "";
        String getterText = "";
        String setterText = "";

        Set set = fields.entrySet();
        Iterator iterator = set.iterator();

        while (iterator.hasNext()) {

            Map.Entry entry = (Map.Entry) iterator.next();
            PojoField pojoField = (PojoField) entry.getValue();

            fieldText += fieldTemplate.replace("[field_type]", pojoField.getType()).replace("[field_name]",
                    pojoField.getName());

            getterText += getterTemplate.replace("[field_type]", pojoField.getType()).replace("[field_name]",
                    capitalizeFirstChar(pojoField.getName()));

            setterText += setterTemplate.replace("[field_type]", pojoField.getType()).replace("[field_name]",
                    capitalizeFirstChar(pojoField.getName()));
        }

        classText = classTemplate.replace("[class_name]", className).replace("[fields_content]", fieldText)
                .replace("[getter_content]", getterText).replace("[setter_content]", setterText);

        System.out.println(classText);
    }

    private String capitalizeFirstChar(String text) {
        return text.substring(0, 1).toUpperCase() + text.substring(1);
    }
}
